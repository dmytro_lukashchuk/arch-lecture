package group.spd.arch.notification.sender;

import group.spd.arch.notification.message.Message;
import group.spd.arch.notification.message.TypedMessageProvider;
import org.apache.commons.text.StringSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class NotificationSenderImpl implements NotificationSender {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final TypedMessageProvider messageProvider;

    public NotificationSenderImpl(TypedMessageProvider messageProvider) {
        this.messageProvider = messageProvider;
    }

    @Override
    @RabbitListener(queues = "NotificationQueue")
    public void sendNotification(NotificationSendEvent event) {
        messageProvider.getByType(event.getNotificationType())
                .ifPresent(message -> sendMessage(message, event));
    }

    private void sendMessage(Message message, NotificationSendEvent event) {
        final StringSubstitutor substitutor = new StringSubstitutor(event.getParams());
        final String content = substitutor.replace(message.getContent());

        logger.info("Send notification to email {}", event.getAssigneeEmail());
        logger.info("==================================================");
        logger.info(content);
        logger.info("==================================================");
    }
}
