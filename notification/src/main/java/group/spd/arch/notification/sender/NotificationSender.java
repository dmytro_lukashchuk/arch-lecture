package group.spd.arch.notification.sender;

public interface NotificationSender {

    void sendNotification(NotificationSendEvent event);
}
