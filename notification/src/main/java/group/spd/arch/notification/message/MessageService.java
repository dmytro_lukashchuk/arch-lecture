package group.spd.arch.notification.message;

import group.spd.arch.error.NotFoundException;
import group.spd.arch.error.ValidationException;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    private final MessageRepository repository;

    public MessageService(MessageRepository repository) {
        this.repository = repository;
    }

    public Message getById(int id) {
        return repository.getById(id)
                .orElseThrow(() -> new NotFoundException("Can't find message with id " + id));
    }

    public Message create(Message message) {
        validate(message);
        return repository.save(message);
    }

    public void update(Message message) {
        validate(message);
        final int updatedRows = repository.update(message);
        if (updatedRows == 0) {
            throw new NotFoundException("Can't find message with id " + message.getId());
        }
    }

    private void validate(Message message) {
        if (message.getType() == null || message.getType().isBlank()) {
            throw new ValidationException("Type is required");
        }
        if (message.getContent() == null || message.getContent().isBlank()) {
            throw new ValidationException("Type is required");
        }
    }
}
