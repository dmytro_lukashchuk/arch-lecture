package group.spd.arch.person;

import java.util.List;

public interface PersonRepository {

	Person save(Person person);

	List<Person> getAll();
}
