package group.spd.arch.task;

import group.spd.arch.error.ConflictException;
import group.spd.arch.error.ValidationException;
import group.spd.arch.person.Person;
import group.spd.arch.person.PersonProvider;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static java.time.LocalDate.now;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TaskServiceTest {

	@Mock
	private TaskRepository taskRepository;

	@Mock
	private PersonProvider personProvider;

	@Mock
	private TaskNotificationSender notificationService;

	@InjectMocks
	private TaskService taskService;

	@Test
	@DisplayName("Should return all tasks from repository")
	void testGetAll() {
		final List<Task> expectedTasks = List.of(new Task());
		when(taskRepository.getAll()).thenReturn(expectedTasks);

		final List<Task> tasks = taskService.getAll();
		assertSame(expectedTasks, tasks);
	}

	@Test
	@DisplayName("Should fail validation for empty description")
	void validateDescription() {
		final Task task = validTask();
		task.setDescription("    ");

		assertThrows(ValidationException.class, () -> taskService.save(task));
	}

	@Test
	@DisplayName("Should fail validation for empty deadline")
	void validateEmptyDeadline() {
		final Task task = validTask();
		task.setDeadline(null);

		assertThrows(ValidationException.class, () -> taskService.save(task));
	}

	@Test
	@DisplayName("Should fail validation if deadline is in the past")
	void validateDeadlineInFuture() {
		final Task task = validTask();
		task.setDeadline(now().minusDays(1));

		assertThrows(ValidationException.class, () -> taskService.save(task));
	}

	@Test
	@DisplayName("Should throw exception on assign for not existing person")
	void validateAssignee() {
		final Task task = validTask();
		when(personProvider.getById(task.getAssigneeId())).thenReturn(Optional.empty());

		assertThrows(ConflictException.class, () -> taskService.save(task));
	}

	@Test
	@DisplayName("Should save valid task")
	void testSave() {
		final Task task = validTask();
		when(personProvider.getById(task.getAssigneeId())).thenReturn(Optional.of(new Person()));

		taskService.save(task);
		verify(taskRepository).save(task);
	}

	@Test
	@DisplayName("Should send event on task creation")
	void sendNotification() {
		final Task task = validTask();
		final Person person = new Person();
		person.setEmail("some@email.com");

		when(personProvider.getById(task.getAssigneeId())).thenReturn(Optional.of(person));
		taskService.save(task);

		final ArgumentCaptor<TaskCreatedEvent> captor = ArgumentCaptor.forClass(TaskCreatedEvent.class);
		verify(notificationService).onTaskCreated(captor.capture());

		final TaskCreatedEvent event = captor.getValue();
		assertAll(
				() -> assertEquals(person.getEmail(), event.getAssigneeEmail()),
				() -> assertEquals(task.getDescription(), event.getTaskDescription()),
				() -> assertEquals(task.getDeadline(), event.getDeadline())
		);
	}

	private Task validTask() {
		final Task task = new Task();
		task.setAssigneeId(42);
		task.setDescription("Some task description");
		task.setDeadline(now().plusDays(2));
		return task;
	}
}